package com.example.di

import com.example.dao.DAOFacadeImpl
import com.example.service.ArticleService
import com.example.service.ArticleServiceImpl
import com.example.service.AzureEventHubService
import com.example.service.AzureEventHubServiceImpl
import org.koin.dsl.module
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.Disposables.single

val myModule = module {
    single { ArticleServiceImpl(get()) as ArticleService }
    single { DAOFacadeImpl() }
    single { AzureEventHubServiceImpl() as AzureEventHubService }

}