package com.example.db

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.config.*
import org.ktorm.database.Database

object DatabaseConnection {

    private val appConfig = HoconApplicationConfig(ConfigFactory.load())
    val dbUrl = appConfig.property("db.jdbcUrl").getString()
    val dbUser =  appConfig.property("db.dbUser").getString()
    val dbPassword = appConfig.property("db.dbPassword").getString()

    private val config = HikariConfig().apply {
        jdbcUrl = appConfig.property("db.jdbcUrl").getString()
        username = appConfig.property("db.dbUser").getString()
        password = appConfig.property("db.dbPassword").getString()
        isAutoCommit = false
        maximumPoolSize = 15
        transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        maxLifetime = 1800000
        minimumIdle =  10
    }
    private val dataSource = HikariDataSource(config)
    val ktromHikariDatabaseConn = Database.connect(dataSource)
    val ktorHikariDatabaseConn = org.jetbrains.exposed.sql.Database.connect(dataSource)





}