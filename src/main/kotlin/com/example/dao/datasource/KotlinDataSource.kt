package com.example.dao.datasource

import com.typesafe.config.ConfigFactory
import io.ktor.server.application.*
import io.ktor.server.config.*
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

fun Application.initDBConn() {
        val appConfig = HoconApplicationConfig(ConfigFactory.load())
        val dbUrl = appConfig.property("db.jdbcUrl").getString()
        val dbUser =  appConfig.property("db.dbUser").getString()
        val dbPassword = appConfig.property("db.dbPassword").getString()
        val driverClassName = appConfig.property("db.driverClassName").getString()
        //   val database = Database.connect (dbUrl, driverClassName)
        val database=Database.connect(dbUrl, driver = "org.postgresql.Driver" , user = dbUser, password = dbPassword)
      /*  val dbUrl = "jdbc:postgresql://psg-tddev-de-ci-bpy-02.postgres.database.azure.com:5432/TDLBillPay"
        val dbUser = "appuser@psg-tddev-de-ci-bpy-02"
        val dbPass = "TDLBpay@123!"
        val database=Database.connect(dbUrl, driver = "org.postgresql.Driver", user = dbUser, password = dbPass) */

    }
/*

    fun connect(): Connection? {
        var conn: Connection? = null
        try {
          */
/*  val dbUrl = appConfig.property("db.jdbcUrl").getString()
            val dbUser =  appConfig.property("db.dbUser").getString()
            val dbPass = appConfig.property("db.dbPassword").toString()
           *//*

            val dbUrl = "jdbc:postgresql://psg-tddev-de-ci-bpy-02.postgres.database.azure.com:5432/TDLBillPay"
            val dbUser = "appuser@psg-tddev-de-ci-bpy-02"
            val dbPass = "TDLBpay@123!"
            Class.forName("org.postgresql.Driver")
            conn = DriverManager.getConnection(dbUrl, dbUser, dbPass)
            println("Connected to the PostgreSQL server successfully.")
        } catch (e: SQLException) {
            println(e.printStackTrace())
        }
        return conn
    }
*/
/*
    suspend fun <T> dbQuery(block: suspend () -> T): T =
            newSuspendedTransaction(Dispatchers.IO) { block() }
}*/