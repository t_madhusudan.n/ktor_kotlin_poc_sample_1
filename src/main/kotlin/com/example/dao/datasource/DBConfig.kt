package com.example.dao.datasource

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.application.*
import io.ktor.server.config.*
import io.ktor.util.*
import org.jetbrains.exposed.sql.Database
import org.slf4j.LoggerFactory

const val HIKARI_CONFIG_KEY = "ktor.hikariconfig"

//@KtorExperimentalAPI
fun Application.initKtorHikariDB(): Database {
    //val config: HikariConfig = HikariConfig()
    //var ds: HikariDataSource? = null
    val appConfig = HoconApplicationConfig(ConfigFactory.load())
    val dbUrl = appConfig.property("db.jdbcUrl").getString()
    val dbUser =  appConfig.property("db.dbUser").getString()
    val dbPassword = appConfig.property("db.dbPassword").getString()

   /* config.jdbcUrl =  dbUrl
    config.username = dbUser
    config.password = dbPassword
    config.addDataSourceProperty("cachePrepStmts", "true")
    config.addDataSourceProperty("prepStmtCacheSize", "250")
    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")*/

    val config = HikariConfig().apply {
        jdbcUrl = appConfig.property("db.jdbcUrl").getString()
        username = appConfig.property("db.dbUser").getString()
        password = appConfig.property("db.dbPassword").getString()
        isAutoCommit = false
        maximumPoolSize = 15
        transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        maxLifetime = 1800000
        minimumIdle =  10
       // validate()
    }

    //val configPath = environment.config.property(HIKARI_CONFIG_KEY).getString()
    //print(configPath)
    //val dbConfig = HikariConfig(config)
    val dataSource = HikariDataSource(config)
  //  Database.connect(dataSource)
    //createTables()
    LoggerFactory.getLogger(Application::class.simpleName).info("Initialized Database with Ktor Orm plugin and hikari CP")
    return Database.connect(dataSource)
}