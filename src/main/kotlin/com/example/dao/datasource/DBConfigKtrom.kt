package com.example.dao.datasource

import com.example.models.ktrom.entity.ArticlesEntity
import com.example.models.ktrom.entity.ItemEntity
import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.application.*
import io.ktor.server.config.*
import org.ktorm.database.Database
import org.ktorm.dsl.from
import org.ktorm.dsl.innerJoin
import org.slf4j.LoggerFactory


//@KtorExperimentalAPI
fun Application.initKtromHikariDB(): Database {
    val appConfig = HoconApplicationConfig(ConfigFactory.load())
    val config = HikariConfig().apply {
        jdbcUrl = appConfig.property("db.jdbcUrl").getString()
        username = appConfig.property("db.dbUser").getString()
        password = appConfig.property("db.dbPassword").getString()
        isAutoCommit = false
        maximumPoolSize = 15
        transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        maxLifetime = 180000000
        minimumIdle =  10
    }

    val dataSource = HikariDataSource(config)
    LoggerFactory.getLogger(Application::class.simpleName).info("Initialized Database using Ktrom plugin with Hikari CP")
    val database = Database.connect(dataSource)
    return database
}