package com.example.dao

import com.example.models.Article
import com.example.models.ArticleEntity
import org.jetbrains.exposed.sql.SizedIterable

interface DAOFacade {
    //fun allArticles(): List<Article>
   // fun article(id: Int): Article?
    //suspend fun addNewArticle(title: String, body: String): Article?
    //suspend fun editArticle(id: Int, title: String, body: String): Boolean
    //suspend fun deleteArticle(id: Int): Boolean
    fun getAllArticles(): Iterable<Article>
    fun getAllArticlesCount(): Long
    //fun getAllArticles(): SizedIterable<ArticleEntity>
}