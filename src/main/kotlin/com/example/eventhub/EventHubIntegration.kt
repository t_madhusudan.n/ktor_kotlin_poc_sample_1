package com.example.eventhub

import com.azure.messaging.eventhubs.EventData
import com.azure.messaging.eventhubs.EventDataBatch
import com.azure.messaging.eventhubs.EventHubClientBuilder
import com.azure.messaging.eventhubs.EventHubProducerClient
import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.request.*

fun Application.callAZEventHub() {

    try {
        val connectionString = "Endpoint=sb://ehn-tddev-de-ci-dap-05.servicebus.windows.net/;SharedAccessKeyName=pol-send-s-billpay-generated;SharedAccessKey=EAvCEgOxXBKUtpbB6TxFM0tXKovFvpDx+fFvunn39eE=;EntityPath=s-billpay-generated"
        val hubName = "s-billpay-generated"
        val eventData = "{\n" +
                "\t\t\t\t\t\t  \"eventType\": \"bill_reminder\",\n" +
                "\t\t\t\t\t\t  \"customerHash\": \"d3aa00c65a78ea4d396b909677310ec9\",\n" +
                "\t\t\t\t\t\t  \"version\": \"1.0\",\n" +
                "\t\t\t\t\t\t  \"eventDetails\": [\n" +
                "\t\t\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\t  \"title\": \"You have some bills due\",\n" +
                "\t\t\t\t\t\t\t  \"billerAccountId\": \"BPAN000000000000124399\",\n" +
                "\t\t\t\t\t\t\t  \"description\": \"You have some bills pending in your cart\",\n" +
                "\t\t\t\t\t\t\t  \"cta\": [\n" +
                "\t\t\t\t\t\t\t\t{\n" +
                "\t\t\t\t\t\t\t\t  \"ctaType\": \"pay_now\",\n" +
                "\t\t\t\t\t\t\t\t  \"ctaText\": \"Pay Now\",\n" +
                "\t\t\t\t\t\t\t\t  \"conversationText\": \"Pay Now\",\n" +
                "\t\t\t\t\t\t\t\t  \"style\": \"selected\",\n" +
                "\t\t\t\t\t\t\t\t  \"ctaUrlPWA\": \"https://dev-r2.tatadigital.com/paybill\",\n" +
                "\t\t\t\t\t\t\t\t  \"ctaUrlAndroid\": \"https://dev-r2.tatadigital.com/paybill\",\n" +
                "\t\t\t\t\t\t\t\t  \"ctaUrliOS\": \"https://dev-r2.tatadigital.com/paybill\"\n" +
                "\t\t\t\t\t\t\t\t}\n" +
                "\t\t\t\t\t\t\t  ]\n" +
                "\t\t\t\t\t\t\t}\n" +
                "\t\t\t\t\t\t  ]\n" +
                "\t\t\t\t\t\t}"
        log.info("Connection String =========:$connectionString")
        log.info("event json payload=========: $eventData")
        val producer: EventHubProducerClient = EventHubClientBuilder().connectionString(connectionString, hubName)
                .buildProducerClient()

        val eventDataBatch: EventDataBatch = producer.createBatch()
        eventDataBatch.tryAdd(EventData(eventData))
        producer.send(eventDataBatch)
        log.info("event json send success=========:")
        producer.close()
    }catch ( e : Exception) {
        log.error("error in pushChannelIntegrationData::ChannelIntegrationServiceImpl.java", e);
    }

}
