package com.example.models.ktrom.entity

import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar

object ArticlesEntity: Table<Article>("articles"){
    val id= int("id").primaryKey().bindTo { it.id }
    val title=varchar("title").bindTo { it.title }
    val body=varchar("body").bindTo { it.body }

}