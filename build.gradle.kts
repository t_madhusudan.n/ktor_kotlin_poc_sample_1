val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project

plugins {
    application
    kotlin("jvm") version "1.7.10"
    id("io.ktor.plugin") version "2.1.1"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.7.10"
}

group = "com.example"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.tomcat.EngineMain")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
    //gradlePluginPortal()
    /*maven {
        url = uri("http://repo.maven.apache.org/maven2")
    }*/
}

dependencies {
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-call-logging-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-tomcat-jvm:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")


    //Azure Eventhub integration plugin
    implementation("com.azure:azure-messaging-eventhubs:5.13.0")

    //HikariCP plugin - for lightweight JDBC connection
    implementation("com.zaxxer:HikariCP:5.0.1")

    // Kotlin ORM plugins
    implementation("org.jetbrains.exposed:exposed-core:0.39.2")
    implementation("org.jetbrains.exposed:exposed-dao:0.39.2")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.39.2")
    //Postgre sql JDBC driver plugin
    implementation("org.postgresql:postgresql:42.5.0")

    // Dependency injection Koin
    implementation("io.insert-koin:koin-ktor:3.2.1")
    implementation("io.insert-koin:koin-logger-slf4j:3.2.1")


    //Ktrom Plugin
    // https://mvnrepository.com/artifact/org.ktorm/ktorm-core
    implementation("org.ktorm:ktorm-core:3.5.0")
    // https://mvnrepository.com/artifact/me.liuwj.ktorm/ktorm-support-postgresql
    implementation("me.liuwj.ktorm:ktorm-support-postgresql:3.1.0")
    //implementation ("org.ktorm:ktorm-support-postgresql:3.5.0")



    testImplementation("io.ktor:ktor-server-tests-jvm:$ktor_version")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
}